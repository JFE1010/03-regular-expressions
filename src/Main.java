import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

	public static void main(String[] args) {
		
		// TODO: Use a regular expression to replace all whitespace with a single space 
		String s1 = "this   string   has a lot   of extra spaces";
		System.out.println(s1.replaceAll("\\s+"," "));
		
		
		// TODO: Use the String.split method and a regular expression to split the comma separated values in the string below
		// and display each part
		String s2 = "a, ab , abc, abcd  , abcde";
		String[] words = s2.split("\\b");
		for (String word : words) {
			if(word.matches("\\w+")) {
				System.out.println(word);
			}
		}
		
		
		// TODO: Use the Pattern and Matcher classes to find and display all the e-mail addresses in the string below
		String s3 = "Emails: joe@yahoo.com; matt@gmail.com, test@daugherty.com";
		Pattern pattern = Pattern.compile("\\w+@\\w+.\\w+");
		Matcher matcher = pattern.matcher(s3);
		while (matcher.find()) {
			System.out.println(matcher.group());
		}
	}
}